package pl.atom.game.simple.character.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.util.List.copyOf;

public class InMemoryCharacterRepository implements CharacterRepository {

    private final Map<Long, Character> charactersMap = new HashMap<>();

    @Override
    public Character save(Character character) {
        if(charactersMap.get(character.getInternalId()) == null) {
            character.setInternalId((long)charactersMap.size());
        }
        charactersMap.put(character.getInternalId(), character);
        return character;
    }

    @Override
    public List<Character> findAll() {
        return copyOf(charactersMap.values());
    }

    @Override
    public Character getById(UUID id) {
        return charactersMap.values()
                .stream()
                .filter(c -> c.getId().equals(id))
                .findFirst()
                .orElse(null);
    }
}
