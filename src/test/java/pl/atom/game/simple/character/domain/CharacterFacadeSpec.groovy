package pl.atom.game.simple.character.domain

import pl.atom.game.simple.character.dto.CharacterDto
import spock.lang.Specification
import spock.lang.Subject

class CharacterFacadeSpec extends Specification {

    CharacterRepository repository = new InMemoryCharacterRepository()

    CharacterCreationService creationService = new CharacterCreationService(repository)
    CharacterFetchingService fetchingService = new CharacterFetchingService(repository)
    CharacterUpdatingService updatingService = new CharacterUpdatingService(repository)

    @Subject
    CharacterFacade facade = new CharacterFacade(creationService, fetchingService, updatingService)

    def "should be able to create character"() {
        when:
        CharacterDto characterDto = facade.createCharacter('TEST')
        then:
        facade.getAllCharacters().id.contains(characterDto.id)
    }

    def "should be able to update character"() {
        given:
            CharacterDto character = facade.createCharacter('TEST')
        when:
            CharacterDto newCharacter = CharacterDto.builder()
                    .id(character.id)
                    .name(character.name)
                    .level(character.level + 5)
                    .experience(character.experience + 10)
                    .build()
            facade.updateCharacter(newCharacter)
        then:
            with(facade.getAllCharacters().stream().filter({ c -> c.getId() == character.id }).findFirst().get()) {
                level == character.level + 5
                experience == character.experience + 10
            }
    }
}
