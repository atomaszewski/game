package pl.atom.game.simple.engine.domain

import pl.atom.game.simple.character.dto.CharacterDto
import pl.atom.game.simple.engine.actions.Reaction
import pl.atom.game.simple.engine.game.actions.PlayGameAction
import pl.atom.game.simple.start.actions.LoadGameAction
import pl.atom.game.simple.start.actions.StartingScreenAction
import spock.lang.Specification
import spock.lang.Subject

class LoadGameActionHandlerSpec extends Specification {

    @Subject
    LoadGameActionHandler handler = new LoadGameActionHandler()

    def 'should handle character selection'() {
        given:
            LoadGameAction loadGameAction = new LoadGameAction(charactersList())
        when:
            def result = handler.handle(loadGameAction, new Reaction('0'))
        then:
            result.class == PlayGameAction
            ((PlayGameAction) result).getCharacterDto().id == UUID.fromString('378cac76-ee85-4710-8626-50b14be915b9')
    }

    def 'should handle back selection'() {
        given:
            LoadGameAction loadGameAction = new LoadGameAction(charactersList())
        when:
            def result = handler.handle(loadGameAction, new Reaction('q'))
        then:
            result.class == StartingScreenAction
    }

    List<CharacterDto> charactersList() {
        List.of(
                CharacterDto.builder()
                        .name('First')
                        .id(UUID.fromString('378cac76-ee85-4710-8626-50b14be915b9'))
                        .level(1)
                        .experience(0)
                        .build(),
                CharacterDto.builder()
                        .name('Second')
                        .id(UUID.fromString('378cac76-ee85-4710-8626-50b14be915b8'))
                        .level(8)
                        .experience(8)
                        .build(),
                CharacterDto.builder()
                        .name('Third')
                        .id(UUID.fromString('378cac76-ee85-4710-8626-50b14be915b7'))
                        .level(5)
                        .experience(1)
                        .build()
        )

    }
}
