package pl.atom.game.lotr;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import pl.atom.game.common.Game;

@Component
@Order(2)
class LotrGame implements Game {

    private final static String NAME = "Lort of the Rings - Epic Bilbo adventure (coming soon)";

    @Override
    public boolean start() {
        System.out.println("Sorry, we didn't write it yet. Coming soon!");
        return false;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
