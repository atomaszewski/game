package pl.atom.game;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.atom.game.common.Game;

import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

@SpringBootApplication
@Slf4j
@AllArgsConstructor
public class GameApplication implements CommandLineRunner {

    private final List<Game> availableGames;

    public static void main(String[] args) {
        SpringApplication.run(GameApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Choose your adventure!");
        Scanner scanner = new Scanner(System.in);
        while (gameSelector(scanner)) { //infinite loop! Keep in mind to have exit way in the body

        }
    }

    private boolean gameSelector(Scanner scanner) {
        printAvailableGames();
        printExitInfo();
        System.out.print("Your choice: ");
        String input = scanner.nextLine();
        boolean result = true;

        switch (input) {
            case "1":
                result = availableGames.get(0).start();
                break;
            case "2":
                result = availableGames.get(1).start();
                break;
            case "q":
            case "Q":
                result = false;
                break;
            default:
                System.err.println("Wrong game number. Please select again");
        }

        return result;
    }

    private void printExitInfo() {
        System.out.println("\n\nq: Nah. I'll skip this time (exit).\n");
    }

    private void printAvailableGames() {
        IntStream.range(0, availableGames.size()).
                forEachOrdered(this::printGame);
    }

    private void printGame(int i) {
        System.out.println((i + 1) + ": " + availableGames.get(i).getName());
    }
}