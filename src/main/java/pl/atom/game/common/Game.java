package pl.atom.game.common;

public interface Game {

    /**
     * Start a game.
     * Do not use System.exit() during the game run.
     * Take care of all exceptions.
     * return false once game is finished
     */
    boolean start();
    String getName();
}
