package pl.atom.game.simple.start.domain;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import pl.atom.game.common.Game;
import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.ActionableHandler;
import pl.atom.game.simple.engine.actions.InvalidReactionException;
import pl.atom.game.simple.engine.actions.Reaction;
import pl.atom.game.simple.engine.domain.SimpleGameEngine;
import pl.atom.game.simple.presentation.*;
import pl.atom.game.simple.start.actions.StartingScreenAction;
import pl.atom.game.simple.start.presentations.WelcomePresentation;

@Component
@Order(1)
@AllArgsConstructor
@Slf4j
class SimpleGame implements Game {

    private final static String NAME = "Simple game";
    private final Presenter presenter;
    private final ActionableHandler actionableHandler;
    private final SimpleGameEngine engine;
    @Override
    public boolean start() {
        presenter.present(new WelcomePresentation());
        Actionable action = new StartingScreenAction();
        while(action.hasNextActionable()) {
            Reaction reaction = actionableHandler.handleActionable(action);
            try {
                action = engine.apply(action, reaction);
            } catch (InvalidReactionException e) {
                log.debug(e.getMessage());
            }
        }
        return false;
    }

    @Override
    public String getName() {
        return NAME;
    }
}
