package pl.atom.game.simple.start.actions;

import org.apache.commons.lang3.StringUtils;
import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.Reaction;

public class StartingScreenAction implements Actionable {

    @Override
    public String getActionMessage() {
        return "What do you want to do? \n\n1. Start a new game\n2. Load previous game\nq. Exit\n\n\nSelect: ";
    }

    @Override
    public boolean isValidReaction(Reaction reaction) {

        switch (reaction.getReaction()) {
            case "1":
            case "2":
            case "q":
            case "Q":
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean hasNextActionable() {
        return true;
    }
}
