package pl.atom.game.simple.start.actions;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import pl.atom.game.simple.character.dto.CharacterDto;
import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.InvalidReactionException;
import pl.atom.game.simple.engine.actions.Reaction;

import java.util.List;
import java.util.stream.IntStream;

@AllArgsConstructor
public class LoadGameAction implements Actionable {

    private final List<CharacterDto> availableCharacters;

    @Override
    public String getActionMessage() {
        StringBuilder basicMessage = new StringBuilder("Choose your character:\n");
        IntStream.range(0, availableCharacters.size())
                .forEachOrdered(i -> basicMessage.append(i).append(".").append(availableCharacters.get(i)).append("\n"));
        basicMessage.append("q: back");
        return basicMessage.toString();
    }

    @Override
    public boolean isValidReaction(Reaction reaction) {
        if(StringUtils.isNumeric(reaction.getReaction())) {
            int intChoice = Integer.parseInt(reaction.getReaction());
            return intChoice >=0 && intChoice < availableCharacters.size();
        } else {
            return "q".equals(reaction.getReaction().toLowerCase());
        }
    }

    @Override
    public boolean hasNextActionable() {
        return true;
    }

    public CharacterDto getCharacterToPlay(Reaction reaction) throws InvalidReactionException {
        if(isValidReaction(reaction)) {
            return availableCharacters.get(Integer.parseInt(reaction.getReaction()));
        }
        throw new InvalidReactionException(reaction);
    }

    public boolean isBackCommand(Reaction reaction) {
        return "q".equals(reaction.getReaction().toLowerCase());
    }
}
