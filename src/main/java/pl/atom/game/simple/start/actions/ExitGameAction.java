package pl.atom.game.simple.start.actions;

import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.Reaction;

public class ExitGameAction implements Actionable {

    @Override
    public String getActionMessage() {
        return null;
    }

    @Override
    public boolean isValidReaction(Reaction reaction) {
        return false;
    }

    @Override
    public boolean hasNextActionable() {
        return false;
    }
}
