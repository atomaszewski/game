package pl.atom.game.simple.start.actions;

import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.InvalidReactionException;
import pl.atom.game.simple.engine.actions.Reaction;

public class NewGameAction implements Actionable {

    @Override
    public String getActionMessage() {
        return "Type your character name (4-10 chars): ";
    }

    @Override
    public boolean isValidReaction(Reaction reaction) {
        String name = reaction.getReaction();
        return name.length() > 3 && name.length() < 11;
    }


    public Actionable getNextActionable(Reaction reaction) throws InvalidReactionException {
        throw new InvalidReactionException(reaction);
    }

    @Override
    public boolean hasNextActionable() {
        return true;
    }
}
