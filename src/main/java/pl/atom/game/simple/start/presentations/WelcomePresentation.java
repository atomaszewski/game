package pl.atom.game.simple.start.presentations;

import pl.atom.game.simple.presentation.Presentable;

public class WelcomePresentation implements Presentable {

    @Override
    public String getPresentationMessage() {
        return "Welcome to my simple game! Enjoy your adventure";
    }
}
