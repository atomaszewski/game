package pl.atom.game.simple.presentation;

public interface Presenter {

    void present(Presentable presentable);

    void presentWarning(Presentable presentable);
}
