package pl.atom.game.simple.presentation;

import org.springframework.stereotype.Component;
import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.ActionableHandler;
import pl.atom.game.simple.engine.actions.Reaction;

import java.util.Scanner;

@Component
class ConsoleInterface implements Presenter, ActionableHandler {

    private final Scanner scanner = new Scanner(System.in);

    @Override
    public void present(Presentable presentable) {
        printToConsole(presentable.getPresentationMessage());
    }

    @Override
    public void presentWarning(Presentable presentable) {
        printToErrorConsole(presentable.getPresentationMessage());
    }

    @Override
    public Reaction handleActionable(Actionable actionable) {
        printToConsole(actionable.getActionMessage());
        String choice = scanner.nextLine();
        return new Reaction(choice);
    }

    private void printToConsole(String text) {
        System.out.println(text);
    }

    private void printToErrorConsole(String text) {
        System.err.println(text);
    }

}
