package pl.atom.game.simple.presentation;

public interface Presentable {

    String getPresentationMessage();
}
