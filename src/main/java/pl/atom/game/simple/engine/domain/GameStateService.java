package pl.atom.game.simple.engine.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;
import pl.atom.game.simple.character.dto.CharacterDto;

@Service
@Setter
@Getter
class GameStateService {

    private CharacterDto characterDto;
}
