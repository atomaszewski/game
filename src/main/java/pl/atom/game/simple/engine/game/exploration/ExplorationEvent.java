package pl.atom.game.simple.engine.game.exploration;

import lombok.Getter;
import pl.atom.game.simple.presentation.Presentable;

@Getter
public abstract class ExplorationEvent implements Presentable {
    private final int experienceGain;

    public ExplorationEvent(int experienceGain) {
        this.experienceGain = experienceGain;
    }
}
