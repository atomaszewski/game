package pl.atom.game.simple.engine.game.fight;

import java.util.Random;

class TigetFightEvent extends FightEvent {

    public TigetFightEvent(int characterLevel) {
        super(new Random().nextInt(characterLevel + 3));
    }

    @Override
    public String getPresentationMessage() {
        return "You have met a might boar with strength " + getMonsterLevel() + "!";
    }
}
