package pl.atom.game.simple.engine.domain;

import org.springframework.stereotype.Component;
import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.InvalidReactionException;
import pl.atom.game.simple.engine.actions.Reaction;
import pl.atom.game.simple.engine.game.actions.PlayGameAction;
import pl.atom.game.simple.start.actions.LoadGameAction;
import pl.atom.game.simple.start.actions.StartingScreenAction;

@Component
class LoadGameActionHandler implements ActionHandler<LoadGameAction>{

    public Actionable handle(LoadGameAction action, Reaction reaction) throws InvalidReactionException {
        if(action.isBackCommand(reaction)) {
            return new StartingScreenAction();
        }
        return new PlayGameAction(action.getCharacterToPlay(reaction));
    }

    @Override
    public Class<LoadGameAction> getHandlerClass() {
        return LoadGameAction.class;
    }
}
