package pl.atom.game.simple.engine.actions;

public interface ActionableHandler {

    Reaction handleActionable(Actionable actionable);
}
