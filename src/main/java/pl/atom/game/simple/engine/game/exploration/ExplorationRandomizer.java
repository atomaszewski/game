package pl.atom.game.simple.engine.game.exploration;

import java.util.Random;

public class ExplorationRandomizer {

   public static ExplorationEvent randomExplorationEvent() {
       if(new Random().nextBoolean()) {
           return new TreasureEvent();
       }
       return new TrapEvent();
   }
}
