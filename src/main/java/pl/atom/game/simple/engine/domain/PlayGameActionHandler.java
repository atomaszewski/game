package pl.atom.game.simple.engine.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.atom.game.simple.character.domain.CharacterFacade;
import pl.atom.game.simple.character.dto.CharacterDto;
import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.InvalidReactionException;
import pl.atom.game.simple.engine.actions.Reaction;
import pl.atom.game.simple.engine.game.actions.PlayGameAction;
import pl.atom.game.simple.engine.game.exploration.ExplorationEvent;
import pl.atom.game.simple.engine.game.fight.FightEvent;
import pl.atom.game.simple.presentation.Presentable;
import pl.atom.game.simple.presentation.Presenter;
import pl.atom.game.simple.start.actions.ExitGameAction;

import static pl.atom.game.simple.engine.game.exploration.ExplorationRandomizer.randomExplorationEvent;
import static pl.atom.game.simple.engine.game.fight.FightRandomizer.randomFightEvent;

@Component
@AllArgsConstructor
class PlayGameActionHandler implements ActionHandler<PlayGameAction>{

    private final Presenter presenter;
    private final CharacterFacade characterFacade;

    public Actionable handle(PlayGameAction actionable, Reaction reaction) throws InvalidReactionException {

        switch (reaction.getReaction()) {
            case "1":
                ExplorationEvent explorationEvent = randomExplorationEvent();
                presenter.present(explorationEvent);
                return new PlayGameAction(getUpdatedCharacter(actionable.getCharacterDto(), explorationEvent));
            case "2":
                FightEvent fightEvent = randomFightEvent(actionable.getCharacterDto().getLevel());
                presenter.present(fightEvent);
                return new PlayGameAction(getUpdatedCharacter(actionable.getCharacterDto(), fightEvent));
            case "3":
                characterFacade.updateCharacter(actionable.getCharacterDto());
                return new PlayGameAction(actionable.getCharacterDto());
            case "q":
            case "Q":
                return new ExitGameAction();
            default:
                throw new InvalidReactionException(reaction);
        }
    }

    @Override
    public Class<PlayGameAction> getHandlerClass() {
        return PlayGameAction.class;
    }

    private CharacterDto getUpdatedCharacter(CharacterDto characterDto, ExplorationEvent explorationEvent) {
        return getUpdatedCharacter(characterDto, explorationEvent.getExperienceGain());
    }

    private CharacterDto getUpdatedCharacter(CharacterDto characterDto, int experienceGain) {
        int characterExp = characterDto.getExperience();
        characterExp += experienceGain;
        if (characterExp < 0) {
            characterExp = 0;
        }
        int characterLevel = characterDto.getLevel();
        while (characterExp > (1 << characterLevel)) {
            characterExp -= 1 << characterLevel;
            characterLevel++;
        }
        return CharacterDto.builder()
                .id(characterDto.getId())
                .name(characterDto.getName())
                .experience(characterExp)
                .level(characterLevel)
                .build();
    }

    private CharacterDto getUpdatedCharacter(CharacterDto characterDto, FightEvent fightEvent) {
        if (fightEvent.getMonsterLevel() <= characterDto.getLevel()) {
            presenter.presentWarning(new WonFightMessage(fightEvent.getMonsterLevel()));
            return getUpdatedCharacter(characterDto, fightEvent.getMonsterLevel());
        }
        presenter.presentWarning(new LostFightMessage(fightEvent.getMonsterLevel()));
        return getUpdatedCharacter(characterDto, fightEvent.getMonsterLevel() * -1);
    }

    private class LostFightMessage implements Presentable {

        private final int experienceLost;

        private LostFightMessage(int experienceLost) {
            this.experienceLost = experienceLost;
        }

        @Override
        public String getPresentationMessage() {
            return "You have lost the fight! You are loosing " + experienceLost + " experience.";
        }
    }

    private class WonFightMessage implements Presentable {

        private final int experienceGain;

        private WonFightMessage(int experienceGain) {
            this.experienceGain = experienceGain;
        }

        @Override
        public String getPresentationMessage() {
            return "You have won the fight! You are gaining " + experienceGain + " experience.";
        }
    }
}
