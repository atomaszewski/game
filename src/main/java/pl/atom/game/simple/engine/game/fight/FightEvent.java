package pl.atom.game.simple.engine.game.fight;

import lombok.Getter;
import pl.atom.game.simple.presentation.Presentable;

@Getter
public abstract class FightEvent implements Presentable {

    private final int monsterLevel;

    public FightEvent(int monsterLevel) {
        this.monsterLevel = monsterLevel;
    }
}
