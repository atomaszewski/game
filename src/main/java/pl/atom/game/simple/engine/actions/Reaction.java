package pl.atom.game.simple.engine.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Reaction {
    private final String reaction;

    public static Reaction defaultReaction() {
        return new Reaction("");
    }
}
