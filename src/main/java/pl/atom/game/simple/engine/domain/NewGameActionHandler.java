package pl.atom.game.simple.engine.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.atom.game.simple.character.domain.CharacterFacade;
import pl.atom.game.simple.character.dto.CharacterDto;
import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.Reaction;
import pl.atom.game.simple.engine.game.actions.PlayGameAction;
import pl.atom.game.simple.start.actions.NewGameAction;

@Component
@AllArgsConstructor
class NewGameActionHandler implements ActionHandler<NewGameAction> {

    private final CharacterFacade characterFacade;

    public Actionable handle(NewGameAction newGameAction, Reaction reaction) {
        CharacterDto character = characterFacade.createCharacter(reaction.getReaction());
        return new PlayGameAction(character);
    }

    @Override
    public Class<NewGameAction> getHandlerClass() {
        return NewGameAction.class;
    }
}
