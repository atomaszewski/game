package pl.atom.game.simple.engine.game.fight;

import java.util.Random;

class BearFightEvent extends FightEvent {

    public BearFightEvent(int characterLevel) {
        super(new Random().nextInt(characterLevel + 2));
    }

    @Override
    public String getPresentationMessage() {
        return "You have met a might boar with strength " + getMonsterLevel() + "!";
    }
}
