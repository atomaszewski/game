package pl.atom.game.simple.engine.game.fight;

import java.util.Random;

public class FightRandomizer {

   public static FightEvent randomFightEvent(int characterLevel) {
       if(new Random().nextBoolean()) {
           return new BearFightEvent(characterLevel);
       }
       return new TigetFightEvent(characterLevel);
   }
}
