package pl.atom.game.simple.engine.domain;

import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.InvalidReactionException;
import pl.atom.game.simple.engine.actions.Reaction;

interface ActionHandler<T extends Actionable> {

    Actionable handle(T actionable, Reaction reaction) throws InvalidReactionException;
    Class<T> getHandlerClass();
}
