package pl.atom.game.simple.engine.game.actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import pl.atom.game.simple.character.dto.CharacterDto;
import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.Reaction;

@AllArgsConstructor
@Getter
public class PlayGameAction implements Actionable {

    private CharacterDto characterDto;

    @Override
    public String getActionMessage() {
        return "You are " + characterDto.toString() + "\nWhat do you want to do:\n1. Explore\n2. Fight monster\n3. Build a camp (save)\nq. Exit";
    }

    @Override
    public boolean isValidReaction(Reaction reaction) {

        switch (reaction.getReaction()) {
            case "1":
            case "2":
            case "3":
            case "q":
            case "Q":
                return true;
            default:
                return false;
        }

    }

    @Override
    public boolean hasNextActionable() {
        return true;
    }
}
