package pl.atom.game.simple.engine.actions;

public class InvalidReactionException extends Exception {

    public InvalidReactionException(Reaction reaction) {
        super("Provided reaction" + reaction.getClass().getName() + " is invalid");
    }
}
