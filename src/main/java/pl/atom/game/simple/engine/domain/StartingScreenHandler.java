package pl.atom.game.simple.engine.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.atom.game.simple.character.domain.CharacterFacade;
import pl.atom.game.simple.character.dto.CharacterDto;
import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.InvalidReactionException;
import pl.atom.game.simple.engine.actions.Reaction;
import pl.atom.game.simple.presentation.Presentable;
import pl.atom.game.simple.presentation.Presenter;
import pl.atom.game.simple.start.actions.ExitGameAction;
import pl.atom.game.simple.start.actions.LoadGameAction;
import pl.atom.game.simple.start.actions.NewGameAction;
import pl.atom.game.simple.start.actions.StartingScreenAction;

import java.util.List;

import static org.springframework.util.CollectionUtils.isEmpty;

@Component
@AllArgsConstructor
class StartingScreenHandler implements ActionHandler<StartingScreenAction>{

    private final CharacterFacade characterFacade;
    private final Presenter presenter;

    public Actionable handle(StartingScreenAction startingScreenAction, Reaction reaction) throws InvalidReactionException {
        switch (reaction.getReaction()) {
            case "1":
                return new NewGameAction();
            case "2":
                return getLoadGameAction();
            case "q":
                return new ExitGameAction();
        }
        throw new InvalidReactionException(reaction);
    }

    @Override
    public Class<StartingScreenAction> getHandlerClass() {
        return StartingScreenAction.class;
    }

    private Actionable getLoadGameAction() {
        List<CharacterDto> allCharacters = characterFacade.getAllCharacters();
        if (isEmpty(allCharacters)) {
            presenter.presentWarning(new NoPreviousGamesMessage());
            return new StartingScreenAction();
        }
        return new LoadGameAction(allCharacters);
    }

    private class NoPreviousGamesMessage implements Presentable {

        @Override
        public String getPresentationMessage() {
            return "No previous games found!";
        }
    }
}
