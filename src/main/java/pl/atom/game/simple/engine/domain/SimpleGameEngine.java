package pl.atom.game.simple.engine.domain;

import org.springframework.stereotype.Component;
import pl.atom.game.simple.engine.actions.Actionable;
import pl.atom.game.simple.engine.actions.InvalidReactionException;
import pl.atom.game.simple.engine.actions.Reaction;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toMap;

@Component
public class SimpleGameEngine {

    private final Map<Class, ActionHandler> actionHandlerMap;

    public SimpleGameEngine(List<ActionHandler> actionHandlers) {
        this.actionHandlerMap = actionHandlers.stream()
                .collect(toMap(ActionHandler::getHandlerClass, handler -> handler));
    }

    public Actionable apply(Actionable actionable, Reaction reaction) throws InvalidReactionException {
        ActionHandler handler = Optional.ofNullable(actionHandlerMap.get(actionable.getClass())).orElseThrow(() ->
                new InvalidReactionException(reaction));
        return handler.handle(actionable, reaction);
    }


}
