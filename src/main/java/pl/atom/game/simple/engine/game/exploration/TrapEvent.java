package pl.atom.game.simple.engine.game.exploration;

import java.util.Random;

class TrapEvent extends ExplorationEvent {

    TrapEvent() {
        super(new Random().nextInt(10) * -1);
    }

    @Override
    public String getPresentationMessage() {
        int experienceLoss = getExperienceGain() * -1;
        return "You fell into a trap! You are loosing " + experienceLoss + " experience";
    }
}
