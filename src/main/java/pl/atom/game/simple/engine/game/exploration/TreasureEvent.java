package pl.atom.game.simple.engine.game.exploration;

import java.util.Random;

class TreasureEvent extends ExplorationEvent {

    TreasureEvent() {
        super(new Random().nextInt(20));
    }

    @Override
    public String getPresentationMessage() {
        return "You found a treasure! You gain " + getExperienceGain() + " experience!";
    }
}
