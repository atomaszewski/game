package pl.atom.game.simple.engine.actions;

public interface Actionable {

    String getActionMessage();
    boolean isValidReaction(Reaction reaction);
    default Reaction getInvalidAction() {
        return new Reaction("");
    }

    boolean hasNextActionable();
}
