package pl.atom.game.simple.character.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import pl.atom.game.simple.character.dto.CharacterDto;

import javax.persistence.*;

import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Getter
@Accessors(chain = true)
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class Character {

    @Id
    @GeneratedValue(strategy= AUTO)
    private Long internalId;
    @Column(nullable = false)
    private UUID id;
    @Column(nullable = false)
    private String name;
    private int level = 1;
    private int experience = 0;

    public Character(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    CharacterDto dto() {
        return CharacterDto.builder()
                .id(id)
                .name(name)
                .experience(experience)
                .level(level)
                .build();
    }
}
