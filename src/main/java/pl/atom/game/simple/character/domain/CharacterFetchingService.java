package pl.atom.game.simple.character.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.atom.game.simple.character.dto.CharacterDto;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
class CharacterFetchingService {

    private final CharacterRepository repository;

    List<CharacterDto> fetchAllCharacters() {
        return repository.findAll()
                .stream()
                .map(Character::dto)
                .collect(toList());
    }
}
