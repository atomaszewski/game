package pl.atom.game.simple.character.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.atom.game.simple.character.dto.CharacterDto;

@Service
@AllArgsConstructor
class CharacterUpdatingService {

    private final CharacterRepository repository;

    void update(CharacterDto characterDto) {
        Character savedVersion = repository.getById(characterDto.getId());
        savedVersion.setExperience(characterDto.getExperience());
        savedVersion.setLevel(characterDto.getLevel());
        repository.save(savedVersion);
    }
}
