package pl.atom.game.simple.character.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.atom.game.simple.character.dto.CharacterDto;

import java.util.UUID;

@Service
@AllArgsConstructor
class CharacterCreationService {

    private final CharacterRepository repository;

    CharacterDto createNewPlayer(String name) {
        UUID playerId = UUID.randomUUID();
        Character newCharacter = new Character(playerId, name);
        newCharacter = repository.save(newCharacter);
        return newCharacter.dto();
    }
}
