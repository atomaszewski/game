package pl.atom.game.simple.character.dto;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class CharacterDto {

    private UUID id;
    private String name;
    private int experience;
    private int level;

    public String toString() {
        return name + ": level " + level  + "(" + experience + " exp) character";
    }
}
