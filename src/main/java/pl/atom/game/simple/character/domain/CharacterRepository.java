package pl.atom.game.simple.character.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

interface CharacterRepository extends Repository<Character, Long> {

    Character save(Character character);

    @Query("SELECT c FROM Character c ORDER BY c.internalId")
    List<Character> findAll();

    @Query("SELECT c FROM Character c WHERE c.id = :id")
    Character getById(@Param("id") UUID id);
}
