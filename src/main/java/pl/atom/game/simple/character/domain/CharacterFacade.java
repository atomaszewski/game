package pl.atom.game.simple.character.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.atom.game.simple.character.dto.CharacterDto;

import java.util.List;

@Component
@AllArgsConstructor
public class CharacterFacade {

    private final CharacterCreationService creationService;
    private final CharacterFetchingService fetchingService;
    private final CharacterUpdatingService updatingService;

    public CharacterDto createCharacter(String name) {
        return creationService.createNewPlayer(name);
    }

    public List<CharacterDto> getAllCharacters(){
        return fetchingService.fetchAllCharacters();
    }

    public void updateCharacter(CharacterDto characterDto) {
        updatingService.update(characterDto);
    }
}
