This is simple game project. 
It allows to have multiple games in one application.
Currently there is one game: `Simple Game` playable, but there is a second one just to show possiblity (not implemented any playable functions)

`Simple Game` requires PostgreSQL DB `game` to run on port 5436 with user `root/root`. 
For convinience there is `docker-compose.yml` file which allows to create required DB instance using Docker.

To run the application user can build using gradlew `./gradlew bootJar` command and then run `game-1.0.jar` from ./build/libs directory using Java12